/*    */ package name.khartn.kcc_6;
/*    */ 
/*    */ import javax.swing.JEditorPane;
/*    */ import org.apache.commons.lang.StringUtils;
/*    */ import org.openide.cookies.EditorCookie;
/*    */ import org.openide.nodes.Node;
/*    */ import org.openide.util.HelpCtx;
/*    */ import org.openide.util.NbBundle;
/*    */ import org.openide.util.actions.CookieAction;
/*    */ 
/*    */ public final class ToSentenceCase extends CookieAction
/*    */ {
/*    */   protected void performAction(Node[] activatedNodes)
/*    */   {
/* 15 */     EditorCookie c = (EditorCookie)activatedNodes[0].getCookie(EditorCookie.class);
/* 16 */     if (c != null) {
/* 17 */       JEditorPane[] panes = c.getOpenedPanes();
/* 18 */       if (panes.length > 0) {
/* 19 */         String selection = panes[0].getSelectedText();
/* 20 */         int start = panes[0].getSelectionStart();
/* 21 */         int end = panes[0].getSelectionEnd();
/* 22 */         selection = selection.toLowerCase();
/* 23 */         selection = StringUtils.capitalize(selection);
/* 24 */         panes[0].replaceSelection(selection);
/* 25 */         panes[0].select(start, end);
/*    */       }
/*    */     }
/*    */   }
/*    */ 
/*    */   protected int mode() {
/* 31 */     return 8;
/*    */   }
/*    */ 
/*    */   public String getName() {
/* 35 */     return NbBundle.getMessage(ToSentenceCase.class, "CTL_ToSentenceCase");
/*    */   }
/*    */ 
/*    */   protected Class[] cookieClasses() {
/* 39 */     return new Class[] { EditorCookie.class };
/*    */   }
/*    */ 
/*    */   protected void initialize()
/*    */   {
/* 45 */     super.initialize();
/*    */ 
/* 47 */     putValue("noIconInMenu", Boolean.TRUE);
/*    */   }
/*    */ 
/*    */   public HelpCtx getHelpCtx() {
/* 51 */     return HelpCtx.DEFAULT_HELP;
/*    */   }
/*    */ 
/*    */   protected boolean asynchronous() {
/* 55 */     return false;
/*    */   }
/*    */ }

/* Location:           C:\Users\Arthur\Downloads\1174069425381_ChangeCase\rbremner-tocase\netbeans\modules\rbremner-tocase.jar
 * Qualified Name:     rbremner.tocase.ToSentenceCase
 * JD-Core Version:    0.6.0
 */