/*    */ package name.khartn.kcc_6;
/*    */ 
/*    */ import javax.swing.JEditorPane;
/*    */ import org.apache.commons.lang.WordUtils;
/*    */ import org.openide.cookies.EditorCookie;
/*    */ import org.openide.nodes.Node;
/*    */ import org.openide.util.HelpCtx;
/*    */ import org.openide.util.NbBundle;
/*    */ import org.openide.util.actions.CookieAction;
/*    */ 
/*    */ public final class ToProperCase extends CookieAction
/*    */ {
/*    */   protected void performAction(Node[] activatedNodes)
/*    */   {
/* 14 */     EditorCookie c = (EditorCookie)activatedNodes[0].getCookie(EditorCookie.class);
/* 15 */     if (c != null) {
/* 16 */       JEditorPane[] panes = c.getOpenedPanes();
/* 17 */       if (panes.length > 0) {
/* 18 */         String selection = panes[0].getSelectedText();
/* 19 */         int start = panes[0].getSelectionStart();
/* 20 */         int end = panes[0].getSelectionEnd();
/* 21 */         selection = WordUtils.capitalizeFully(selection);
/* 22 */         panes[0].replaceSelection(selection);
/* 23 */         panes[0].select(start, end);
/*    */       }
/*    */     }
/*    */   }
/*    */ 
/*    */   protected int mode() {
/* 29 */     return 8;
/*    */   }
/*    */ 
/*    */   public String getName() {
/* 33 */     return NbBundle.getMessage(ToProperCase.class, "CTL_ToProperCase");
/*    */   }
/*    */ 
/*    */   protected Class[] cookieClasses() {
/* 37 */     return new Class[] { EditorCookie.class };
/*    */   }
/*    */ 
/*    */   protected void initialize()
/*    */   {
/* 43 */     super.initialize();
/*    */ 
/* 45 */     putValue("noIconInMenu", Boolean.TRUE);
/*    */   }
/*    */ 
/*    */   public HelpCtx getHelpCtx() {
/* 49 */     return HelpCtx.DEFAULT_HELP;
/*    */   }
/*    */ 
/*    */   protected boolean asynchronous() {
/* 53 */     return false;
/*    */   }
/*    */ }

/* Location:           C:\Users\Arthur\Downloads\1174069425381_ChangeCase\rbremner-tocase\netbeans\modules\rbremner-tocase.jar
 * Qualified Name:     rbremner.tocase.ToProperCase
 * JD-Core Version:    0.6.0
 */