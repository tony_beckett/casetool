/*    */ package name.khartn.kcc_6;
/*    */ 
/*    */ import javax.swing.JEditorPane;
/*    */ import org.openide.cookies.EditorCookie;
/*    */ import org.openide.nodes.Node;
/*    */ import org.openide.util.HelpCtx;
/*    */ import org.openide.util.NbBundle;
/*    */ import org.openide.util.actions.CookieAction;
/*    */ 
/*    */ public final class ToLowerCase extends CookieAction
/*    */ {
/*    */   protected void performAction(Node[] activatedNodes)
/*    */   {
/* 13 */     EditorCookie c = (EditorCookie)activatedNodes[0].getCookie(EditorCookie.class);
/* 14 */     if (c != null) {
/* 15 */       JEditorPane[] panes = c.getOpenedPanes();
/* 16 */       if (panes.length > 0) {
/* 17 */         String selection = panes[0].getSelectedText();
/* 18 */         int start = panes[0].getSelectionStart();
/* 19 */         int end = panes[0].getSelectionEnd();
/* 20 */         panes[0].replaceSelection(selection.toLowerCase());
/* 21 */         panes[0].select(start, end);
/*    */       }
/*    */     }
/*    */   }
/*    */ 
/*    */   protected int mode() {
/* 27 */     return 8;
/*    */   }
/*    */ 
/*    */   public String getName() {
/* 31 */     return NbBundle.getMessage(ToLowerCase.class, "CTL_ToLowerCase");
/*    */   }
/*    */ 
/*    */   protected Class[] cookieClasses() {
/* 35 */     return new Class[] { EditorCookie.class };
/*    */   }
/*    */ 
/*    */   protected void initialize()
/*    */   {
/* 41 */     super.initialize();
/*    */ 
/* 43 */     putValue("noIconInMenu", Boolean.TRUE);
/*    */   }
/*    */ 
/*    */   public HelpCtx getHelpCtx() {
/* 47 */     return HelpCtx.DEFAULT_HELP;
/*    */   }
/*    */ 
/*    */   protected boolean asynchronous() {
/* 51 */     return false;
/*    */   }
/*    */ }

/* Location:           C:\Users\Arthur\Downloads\1174069425381_ChangeCase\rbremner-tocase\netbeans\modules\rbremner-tocase.jar
 * Qualified Name:     rbremner.tocase.ToLowerCase
 * JD-Core Version:    0.6.0
 */