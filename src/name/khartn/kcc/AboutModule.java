package name.khartn.kcc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;

@ActionID(category = "Edit",
id = "name.khartn.kcc.AboutModule")
@ActionRegistration(displayName = "#CTL_AboutModule")
@ActionReferences({
    @ActionReference(path = "Menu/Edit", position = 3270)
})
@Messages("CTL_AboutModule=KhArtN's Case Changer Help")
public final class AboutModule implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        String message = "Case Toogler - Alt+F5 Alt+F5 \r\n"
                + "sWAP cASE - Alt+F5 NUMPAD 1 \r\n"
                + "toCamelCase - Alt+F5 NUMPAD 2 \r\n"
                + "to lower case - Alt+F5 NUMPAD 3 \r\n"
                + "TO UPPER CASE - Alt+F5 NUMPAD 4 \r\n"
                + "To Proper Case - Alt+F5 NUMPAD 6 \r\n"
                + "To sentence case - Alt+F5 NUMPAD 7 \r\n";
        JOptionPane.showMessageDialog(null,
                message,
                "A KhArtN's Case Changer Help",
                JOptionPane.PLAIN_MESSAGE);

    }
}
