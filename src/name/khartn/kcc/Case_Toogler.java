package name.khartn.kcc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.cookies.EditorCookie;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@ActionID(category = "Edit",
id = "name.khartn.kcc.Case_Toogler")
@ActionRegistration(displayName = "#CTL_Case_Toogler")
@ActionReferences({
    @ActionReference(path = "Menu/Edit", position = 3300, separatorBefore = 3250),
    @ActionReference(path = "Shortcuts", name = "O-F5 O-F5")
})
@Messages("CTL_Case_Toogler=Case Toogler")
public final class Case_Toogler implements ActionListener {

    public static Integer counter;
    public static Integer old_start_pos;
    public static Integer old_end_pos;
    public static String symbolsAtTheEndOfSelectedText;
    public static String symbolsAtTheStartOfSelectedText;
    public static String initialString;

    @Override
    public void actionPerformed(ActionEvent e) {

        Integer position_s = 0;
        Integer position_end = 0;

        if (Case_Toogler.counter == null) {
            Case_Toogler.counter = 0;
            Case_Toogler.old_start_pos = 0;
            Case_Toogler.old_end_pos = 0;
            Case_Toogler.symbolsAtTheEndOfSelectedText = "";
            Case_Toogler.symbolsAtTheStartOfSelectedText = "";
            Case_Toogler.initialString = "";
        }

        Node[] n = TopComponent.getRegistry().getActivatedNodes();
        if (n.length == 1) {
            EditorCookie ec = (EditorCookie) n[0].getCookie(EditorCookie.class);
            if (ec != null) {
                JEditorPane[] panes = ec.getOpenedPanes();
                if (panes.length > 0) {
                    try {

                        if (panes[0].getSelectedText() != null) {

                            String selectedText = panes[0].getSelectedText();

                            if (Case_Toogler.counter == 0) {
                                Case_Toogler.initialString = selectedText;
                            }

                            Case_Toogler.counter++;

//                        JOptionPane.showMessageDialog(null,
//                                "!!!" + Case_Toogler.counter,
//                                "A KhArtN's Case Changer Help",
//                                JOptionPane.PLAIN_MESSAGE);

                            position_s = panes[0].getSelectionStart();
                            position_end = panes[0].getSelectionEnd();
                            String startSymbols = selectedText.substring(0, 2).toLowerCase();
                            String endSymbols = selectedText.substring(selectedText.length() - 2).toLowerCase();
                            Boolean textIsChanged = (!startSymbols.equals(Case_Toogler.symbolsAtTheStartOfSelectedText)
                                    || !endSymbols.equals(Case_Toogler.symbolsAtTheEndOfSelectedText));

                            if (Case_Toogler.old_start_pos != position_s && textIsChanged) {

//                                JOptionPane.showMessageDialog(null,
//                                        "Resetting counter \r\n"
//                                        + (Case_Toogler.old_start_pos != position_s) + " \r\n"
//                                        + (Case_Toogler.old_end_pos != position_end) + " \r\n"
//                                        + (!startSymbols.equals(Case_Toogler.symbolsAtTheStartOfSelectedText)) + " \r\n"
//                                        + (!endSymbols.equals(Case_Toogler.symbolsAtTheEndOfSelectedText)) + " \r\n",
//                                        "A KhArtN's Case Changer Help",
//                                        JOptionPane.PLAIN_MESSAGE);

                                Case_Toogler.old_start_pos = position_s;
                                Case_Toogler.old_end_pos = position_end;

                                Case_Toogler.symbolsAtTheStartOfSelectedText = startSymbols;
                                Case_Toogler.symbolsAtTheEndOfSelectedText = endSymbols;
                                Case_Toogler.counter = 1;
                            }

                            if (Case_Toogler.old_end_pos != position_end && textIsChanged) {

//                                JOptionPane.showMessageDialog(null,
//                                        "Resetting counter \r\n"
//                                        + (Case_Toogler.old_start_pos != position_s) + " \r\n"
//                                        + (Case_Toogler.old_end_pos != position_end) + " \r\n"
//                                        + (!startSymbols.equals(Case_Toogler.symbolsAtTheStartOfSelectedText)) + " \r\n"
//                                        + (!endSymbols.equals(Case_Toogler.symbolsAtTheEndOfSelectedText)) + " \r\n",
//                                        "A KhArtN's Case Changer Help",
//                                        JOptionPane.PLAIN_MESSAGE);

                                Case_Toogler.old_start_pos = position_s;
                                Case_Toogler.old_end_pos = position_end;

                                Case_Toogler.symbolsAtTheStartOfSelectedText = startSymbols;
                                Case_Toogler.symbolsAtTheEndOfSelectedText = endSymbols;
                                Case_Toogler.counter = 1;
                            }

//                            Let's switch cases as Word                            
                            if (Case_Toogler.counter == 1) {
                                selectedText = WordUtils.capitalizeFully(selectedText);
                                selectedText = StringUtils.deleteWhitespace(selectedText);
                                selectedText = StringUtils.uncapitalize(selectedText);
                                panes[0].replaceSelection(selectedText);

//                                JOptionPane.showMessageDialog(null,
//                                        "1 - " + Case_Toogler.counter,
//                                        "A KhArtN's Case Changer Help",
//                                        JOptionPane.PLAIN_MESSAGE);
                            }
                            if (Case_Toogler.counter == 2) {
                                panes[0].replaceSelection(WordUtils.swapCase(selectedText));

//                                JOptionPane.showMessageDialog(null,
//                                        "2 - " + Case_Toogler.counter,
//                                        "A KhArtN's Case Changer Help",
//                                        JOptionPane.PLAIN_MESSAGE);
                            }
                            if (Case_Toogler.counter == 3) {
                                panes[0].replaceSelection(WordUtils.swapCase(selectedText));

//                                JOptionPane.showMessageDialog(null,
//                                        Case_Toogler.counter,
//                                        "A KhArtN's Case Changer Help",
//                                        JOptionPane.PLAIN_MESSAGE);
                            }
                            if (Case_Toogler.counter == 4) {
                                panes[0].replaceSelection(selectedText.toLowerCase());

//                                JOptionPane.showMessageDialog(null,
//                                        Case_Toogler.counter,
//                                        "A KhArtN's Case Changer Help",
//                                        JOptionPane.PLAIN_MESSAGE);
                            }
                            if (Case_Toogler.counter == 5) {
                                panes[0].replaceSelection(selectedText.toUpperCase());

//                                JOptionPane.showMessageDialog(null,
//                                        Case_Toogler.counter,
//                                        "A KhArtN's Case Changer Help",
//                                        JOptionPane.PLAIN_MESSAGE);
                            }
                            if (Case_Toogler.counter == 6) {
                                panes[0].replaceSelection(selectedText.toLowerCase());

//                                JOptionPane.showMessageDialog(null,
//                                        Case_Toogler.counter,
//                                        "A KhArtN's Case Changer Help",
//                                        JOptionPane.PLAIN_MESSAGE);
                            }
                            if (Case_Toogler.counter == 7) {
                                panes[0].replaceSelection(Case_Toogler.initialString);
                                Case_Toogler.counter = 0;

//                                JOptionPane.showMessageDialog(null,
//                                        Case_Toogler.counter,
//                                        "A KhArtN's Case Changer Help",
//                                        JOptionPane.PLAIN_MESSAGE);
                            }

                        }

                        panes[0].select(position_s, position_end);
                    } catch (Exception ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }
        }
    }
}
